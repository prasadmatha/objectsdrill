//const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function defaults(testObject, defaultProps){
    if(typeof(testObject) === undefined || typeof(defaultProps) === undefined || (typeof(testObject) === "object" && Array.isArray(testObject))
    || (typeof(defaultProps) === "object" && Array.isArray(defaultProps)) || Object.keys(testObject).length === 0 || Object.keys(defaultProps).length === 0){
        return {}
    }
    else{
        let replaceKey;
        for (let item in defaultProps)
        {
            replaceKey = item
        }
        for (let item in testObject){
            if (item === replaceKey){
                testObject[item] = defaultProps[item]
                break
            }
        }
        return testObject

        
    }
    
}
module.exports = defaults
