function invert(testObject){
    if(typeof(testObject) !== undefined && (typeof(testObject) === "object" && !Array.isArray(testObject))
    && Object.keys(testObject).length !== 0){
        let result = {}
        let objectKeys = []
        for (let item in testObject){
            objectKeys.push(item)
        }


        for (let item of objectKeys){
            result[testObject[item]] = item
        
        }
        return result
    }
    else{
        return {}
    }
    
    }


module.exports = invert
