function mapObject(testObject,cb){
    if(typeof(testObject) !== undefined && cb !== undefined && (typeof(testObject) === "object" && !Array.isArray(testObject))
    && Object.keys(testObject).length !== 0){
        let result = {}
        for (let item in testObject){
            const object = cb({[item] : testObject[item]})
            result[object[0][0]] = object[0][1]
        }
        return result
    }
    else{
        return {}
    }
}

module.exports = mapObject