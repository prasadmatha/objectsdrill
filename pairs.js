//const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function pairs(testObject){
    if(typeof(testObject) !== undefined && (typeof(testObject) === "object" && !Array.isArray(testObject))
    && Object.keys(testObject).length !== 0){
        let result = []
        for (let item in testObject){
            result.push([item, testObject[item]])
        }
        return result
    }
    else{
        return []
    }
}


module.exports = pairs
